package com.example.demoCaching.dao;

import com.example.demoCaching.model.Elemento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ElementiRepository extends CrudRepository<Elemento, Integer> {

    String SEL_BY_VALORE="SELECT * FROM ELEMENTO WHERE VALORE =:valore";

    @Query(value=SEL_BY_VALORE, nativeQuery=true)
    List<Elemento> selByValore(@Param("valore") String valore);
}
