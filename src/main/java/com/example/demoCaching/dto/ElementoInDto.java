package com.example.demoCaching.dto;

import lombok.Data;

@Data
public class ElementoInDto {
    private String chiave;
    private String valore;
}
